declare

fun {NewNonAbstract WClass Init}
   Class = {Unwrap WClass} 
   Fs = {Map Class.attrs fun {$ X} X#{NewCell _} end}
   S = {List.toRecord state Fs}
   proc {Obj M}
      {Class.methods.{Label M} M S}
   end
in
   if not {Member abstract Class.props} then
	 {Obj Init}
	 Obj
   end
end
