declare
fun {New2 Class}
   Dummy_init = {NewName}
   class Dummy from Class
      meth !Dummy_init()
	 skip
      end
   end
in
   {New Dummy Dummy_init}
end

class T
   attr c 
   meth init(Val)
      c := Val
   end
   meth show()
      {Browse @c}
   end
   meth set(Val)
      c := Val
   end
   meth val($)
      @c
   end
end

T1 = {New2 T}
thread {T1 show} end
thread
   {T1 set(2)}
   {T1 show}
end