declare

class Integer
   attr n
   meth init(Val)
      n := Val
   end
   meth Value($)
      @n
   end
   meth plus(Other)
      n := @n + {Other Value($)}
   end
   meth minus(Other)
      n := @n - {Other Value($)}
   end
   meth mult(Other)
      n := @n * {Other Value($)}
   end
   meth show()
      {Browse @n}
   end
end

IntA = {New Integer init(23)}
IntB = {New Integer init(44)}
{IntA show}
{IntA plus(IntB)}
{IntA show}
{IntA minus({New Integer init(9)})}
{IntA show}
{IntA mult(IntB)}
{IntA show}
