declare
fun {TraceNew2 Class Init}
   Object = {New Class Init}
   TInit = {NewName}
   class Tracer
      attr obj
      meth !TInit(Obj)
	 @obj = Obj
      end
      meth otherwise(M)
	 {Browse entering({Label M})}
	 {@obj M}
	 {Browse exiting({Label M})}
      end
   end
in
   {New Tracer TInit(Object)}
end
