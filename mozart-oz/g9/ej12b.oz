declare

class Complex
   attr real_ imag_
   meth init(Real Imag)
      real_ := Real
      imag_ := Imag
   end
   meth Real($)
      @real_
   end
   meth Imag($)
      @imag_
   end
   meth plus(Other)
      real_ := @real_ + {Other Real($)}
      imag_ := @imag_ + {Other Imag($)}
   end
   meth minus(Other)
      real_ := @real_ - {Other Real($)}
      imag_ := @imag_ - {Other Imag($)}
   end
   meth show()
      {Browse @real_}
      if @imag_ < 0.0 then {Browse "-"}
      else {Browse "+"}
      end
      {Browse @imag_}
      {Browse "i"}
   end
end

C1 = {New Complex init(2.1 ~4.2)}
C2 = {New Complex init(0.1 7.2)}
{C1 show}
{C1 minus(C2)}
{C1 show}