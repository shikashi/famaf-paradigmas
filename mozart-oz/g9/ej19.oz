declare

class Collection
   attr elements
   meth init   % inicializa la coleccion
      elements := nil
   end
   meth put(X)   % inserta X en la coleccion
      elements := X|@elements
   end
   meth get($)   % extrae un elemento y lo devuelve
      case @elements
      of X|Xr then
	 elements:=Xr
	 X
      end
   end
   meth isEmpty($)   % devuelve true si la coleccion es vacia
      @elements == nil
   end
   meth show($)
      @elements
   end
   meth union(C2)
      if {C2 isEmpty($)} then skip
      else
	 {self put({C2 get($)})}
	 {self union(C2)}
      end
   end
end

class SortedCollection from Collection
   meth put(X)
      fun {RecPutA E Ls}
	 L|Lr = Ls
      in
	 if E > L then
	    L|{RecPutA E Lr}
	 else
	    E|Ls
	 end
      end
   in
      elements := {RecPutA X @elements}
   end
   meth get($)
      case @elements of L|Lr then
	 elements := Lr
	 L
      end
   end
end
