declare Complejo
fun {Complejo}
   C = {NewCell nil}
   proc {ToComplex A B} C:=A#B end
   fun {Real}
      local A#_ = @C in A end
   end
   fun {Imag}
      local _#B = @C in B end
   end
   proc {Add D}
      local A#B = @C in
          {ToComplex (A+{D.re}) (B+{D.img})}
      end
   end
   proc {See}
      {Browse @C}
   end
in
   complejo(acomplejo:ToComplex sumar:Add mostrar:See re:Real img:Imag)
end
local C D in
   C = {Complejo}
   %{Browse C}
   {C.acomplejo 5 6}
   {C.mostrar}
   D = {Complejo}
   {D.acomplejo 1 2}
   {D.mostrar}
   {C.sumar D}
   {C.mostrar}
end
