%<BinaryTree> ::= leaf
%               | btree(<Value> <BinaryTree> <BinaryTree>)
%<Value> ::= <Integer>

declare

fun {SumTree T}
   case T
   of leaf then
      0
   [] btree(Val T1 T2) then
      Val + {SumTree T1} + {SumTree T2}
   end
end

fun {SumTreeIte T}
   fun {SumT Acc T}
      case T
      of leaf then
	 Acc
      [] btree(Val TL TR) then
	 {SumT Acc+Val btree(0 TR leaf)}
      end
   end
in
   {SumT 0 T}
end

T1 = leaf
T2 = btree(4 btree(1 leaf leaf) btree(6 leaf btree(2 leaf leaf)))

{Browse {SumTree T1}}
{Browse {SumTreeIte T1}}

{Browse {SumTree T2}}
{Browse {SumTreeIte T2}}
