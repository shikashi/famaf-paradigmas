x = "12"

y = 4

z = x + y  # error: cannot concatenate 'str' and 'int'

w = y + x  # error: unsupported operand types for +: 'int' and 'str'
