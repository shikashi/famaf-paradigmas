def even(x):
    """
    Type: A -> Bool
    """
    if x == 0:
        return True
    else:
        return odd(x - 1)

def odd(x):
    """
    Type: A -> Bool
    """
    if x == 0:
        return False
    else:
        return even(x - 1)


def tail_rec(fun):
    """
    Type: V -> (A -> V)
    Given 'fun' a tail-recursive function, tail_rec wil return a function
    of the same type with the recursion "resolved".
    """
    def tail(fun):
        """
        Type: V -> V
        tail will iteratively "resolve" the recursion; note that 'fun'
        may actually be a value and not a function, in that case it is
        the final resulting value.
        """
        a = fun
        while callable(a):
            a = a()
        return a
    return (lambda x: tail(fun(x)))

def tail_even(x):
    """
    Type: A -> Bool
    Same as 'even' except the recursive case does not make a call, instead
    it returns a value (not yet evaluated) representing the result of the
    recursive call. This is the heart, the basis upon which the whole idiom
    builds.
    """
    if x == 0:
        return True
    else:
        return (lambda: tail_odd(x - 1))

def tail_odd(x):
    """
    Type: A -> Bool
    Same as 'odd' except the recursive case does not make a call, instead
    it returns a value (not yet evaluated) representing the result of the
    recursive call. This is the heart, the basis upon which the whole idiom
    builds.
    """
    if x == 0:
        return False
    else:
        return (lambda: tail_even(x - 1))

even_tr = tail_rec(tail_even)
odd_tr = tail_rec(tail_odd)

print even(9998)  # A call such as this one may fail because of the limited
                  # stack space. Here, more than 9998 stack frames are needed
                  #.at the same time.

print even_tr(998)  # Here the tail-recursion is "resolved". With regards to
                    # this call, stack space is needed at the same time only
                    # for one instance of each tail_rec, tail and tail_even
                    # (plus, possibly, 2 lambdas).
print even_tr(9000)  # Idem.
