/**
 * boom is not tail-recursive because the last operation in a recursive
 * execution is not the recursive call, but the call to the + operator.
 */
def boom(x: Int) : Int =
    if (x==0)
        throw new Exception("boom")
    else
        boom(x-1) + 1

boom(3)
