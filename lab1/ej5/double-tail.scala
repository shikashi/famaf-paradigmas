def f(x: Int) : Int =
    if (x == 0)
        throw new Exception("f")
    else
        g(x-1)

def g(x: Int) : Int =
    if (x == 0)
        throw new Exception("g")
    else
        f(x-1)


g(7)

// We get:
// java.lang.Exception: f
//     at Main$$anon$1.f(double-tail.scala:3)
//     at Main$$anon$1.g(double-tail.scala:11)
//     at Main$$anon$1.f(double-tail.scala:5)
//     at Main$$anon$1.g(double-tail.scala:11)
//     at Main$$anon$1.f(double-tail.scala:5)
//     at Main$$anon$1.g(double-tail.scala:11)
//     at Main$$anon$1.f(double-tail.scala:5)
//     at Main$$anon$1.g(double-tail.scala:11)
//     at Main$$anon$1.<init>(double-tail.scala:14)
//     ...
// Which shows that the recursion was not optimized away.
