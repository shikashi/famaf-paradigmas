/**
 * Scala has static typing.
 */


var x : String = "string"

x = "another"

x = 123.12  // error: type mismatch

println(x)
