/**
 * Java has static typing.
 */


class typing {
    public static void main(String[] args) {
        String x = "string";

        x = "another";

        x = 123.12;  // error: incompatible types

        System.out.println(x);
    }
}
