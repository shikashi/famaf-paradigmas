##
# Ruby has dynamic typing.
#


x = "string"

x = "another"

x = 123.12  # note: not an error

puts x
