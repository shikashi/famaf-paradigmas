// x is evaluated now:
val x = {
    println("x init")
    20
}  // x init

println("...")

println(x)  // 20
println(x)  // 20

// y is lazy, it will not be evaluated until needed:
lazy val y = {
    println("y init")
    50
}

println("...")

// only now is y evaluated:
println(y)  // y init
            // 50
println(y)  // 50

// z is a function, its code will be evaluated at each call:
lazy val z = {
    println("z init")
    () => { println("z here"); 80 }
}

println("...")

// but z is also lazy, so its initialization is evaluated only now:
println(z())  // z init
              // z here
              // 80
println(z())  // z here
              // 80
