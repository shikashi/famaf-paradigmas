/**
 * In general JavaScript does not expose a SAS.
 * On the other hand, some major JavaScript implementations (e.g. Mozilla's
 * SpiderMonkey, Google's V8) support constants (the reference is immutable
 * but not the referred value).
 * ECMAScript, however, hasn't yet standarized constant statements (there
 * are plans for it in ES6).
 */


var x = "1st";
x = "2nd";  // ok
alert("x is " + x);

// attempting to "re-bind" a constant is simply ignored:
const DEATH = "CERTAIN";
DEATH = "likely";  // ignored
alert("DEATH is " + DEATH);  // DEATH is still very much CERTAIN

// a constant cannot be re-declared either:
const DEATH = "late";  // SpiderMonkey: error: redeclaration of const; V8: ignored

// a constant can be left undefined:
const USELESS;  // ok, stupid
USELESS = "hopeless";  // ignored
alert("USELESS is " + USELESS);  // indeed

// an object referenced by a constant can change:
var T = function(a) {
    return {
        member: a
    };
};

const t = T("1st");
t.member = "2nd";
alert("t.member is " + t.member); // t.member is 2nd

// but the reference itself cannot:
t = T("none");  // ignored
alert("t.member is " + t.member);  // t.member is 2nd
