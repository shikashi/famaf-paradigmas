/**
 * In general, Java does not expose a SAS.
 * However, Java allows to declare a variable "final". Once "bound", a
 * final variable cannot be re-assigned. The object referenced by a final
 * variable can change, though. It is permitted to leave a final variable
 * uninitialized.
 */


class T {
    public String member;

    public T(String a) {
        this.member = a;
    }
}

class asa {
    public static void main(String[] args) {
        String x = "1st";
        x = "2nd";  // ok
        System.out.println("x is " + x);  // 2nd

        // a final cannot be re-assigned:
        final String f = "1st";
        f = "2nd";  // error: cannot assign to final
        System.out.println("f is " + f);  // 1st

        // a final can be left uninitialized:
        final String g;  // ok, g uninitialized
        g = "1st";  // ok
        g = "2nd";  // error: g already assigned
        System.out.println("g is " + g);  // 1st

        // the object referenced by a final can change:
        final T p = new T("1st");
        p.member = "2nd";  // ok
        System.out.println("p.member is " + p.member);  // 2nd
    }
}
