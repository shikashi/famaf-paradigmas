/**
 * In general, Scala does not expose a SAS.
 * There are 2 kinds of "variables": those which allow multiple assignment
 * (var) and those which do not (val).
 */


// vars can be assigned multiple times:
var x  = "1st"
x = "2nd"
println("x", x)  // prints "2nd"

// vals are single-assignment entities
// they cannot be "re-bound":
val y = "1st"
y = "2nd"  // error: re-assignment to val

// however, the value referred to by a val is not immutable (the value is
// not a constant, the reference to the value is):
class T(s : String) {
  var member = s
}

val t = new T("1st")
t.member = "2nd"  // ok
println("t.member", t.member)  // prints "2nd"

// a val cannot be in an "unbound" state:
val z  // error: '=' expected
