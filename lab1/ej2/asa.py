"""
In Python there are no single-assignment variables.
Any variable can be "re-bound' any number of times.
"""


x = "1st"
x = "2nd"  # ok

y = 123

x = y  # ok
y = 41  # ok

print x  # prints "123"
print y  # prints "41"
