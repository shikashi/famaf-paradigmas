##
# Ruby does not have single-assignment variables.
# In Ruby, an identifier that starts with a capitalized letter is considered
# a constant reference. Like in the case of Scala's val, the "constness" is
# in the reference and not in the referred object or value (the Object.freeze
# method is an approximation to the latter). However this is not enforced
# by the language: re-binding a "constant" will generate a warning but the
# operation will succeed.
#


x = "1st"
x = "2nd"  # ok

Const = "1st"
Const << " and only"  # ok

puts Const  # prints "1st and only"

Const = "2nd"  # warning

puts Const  # prints "2nd"
