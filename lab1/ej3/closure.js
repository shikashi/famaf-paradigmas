/**
 * JavaScript does support closures. They use static scoping.
 */


var A = "alpha";

function f() {
    document.write(A + "<br>");
}

function g() {
    var A = "gamma";
    f();
    document.write(A + "<br>");
}

g();  // alpha
      // gamma

document.write(A + "<br>");  // alpha

A = "aleph";  // aleph
              // gamma

g();
