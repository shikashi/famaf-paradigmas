"""
Python does support closures and they are of static scope (aka lexical scope).
"""

A = "alpha"

def f():
    print A

def g():
    A = "gamma"
    f()
    print A

g()  # alpha
     # gamma

print A  # alpha

A = "aleph"

g()  # aleph
     # gamma
