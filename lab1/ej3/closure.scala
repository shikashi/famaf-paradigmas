/**
 * Scala does suppport closures. They use static scoping.
 */


var a = "alpha"

def f() = {
    println(a)
}

def g() = {
    val a = "gamma"
    f()
    println(a)
}

g()  // alpha
     // gamma

println(a)  // alpha

a = "aleph"

g()  // aleph
     // gamma
