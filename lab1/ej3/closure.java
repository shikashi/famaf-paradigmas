/**
 * Java does not currently support closures (closures are expected to be
 * included in Java version 8).
 * Java does support a limited form of lexical scope capturing: local
 * classes. Classes defined inside methods are called "local classes"
 * and can refer to final names belonging to the enclosing lexical scope.
 */
