##
# Ruby supports lexical closures when using blocks/lambdas.
# These sport static scoping.
#

a = "alpha"

f = lambda do
    puts a
end

def g(x)
    a = "gamma"
    x.call
    puts a
end

g f  # alpha
     # gamma

puts a  # alpha

a = "aleph"

g f  # aleph
     # gamma
