// print String exclusively
def printlnString(s : String) = println(s)

// implicit conversion from Int to String
implicit def Int2String(i : Int) : String = "%d".format(i)

printlnString("meh")

// convert 123 to "123" using Int2String
printlnString(123)
printlnString(123 + "456")

//printlnString(12.2)  // error: expected String

// - - - - - - - - - - - - - - - - - - - - - - - - - - -

// print Int exclusively
def printlnInt(i : Int) = println(i)

class MyInt(val i : Int)  // dummy

// implicit conversion from MyInt to Int
implicit def MyInt2Int(mi : MyInt) : Int = mi.i

// convert MyInt(15) to 15 using MyInt2Int
printlnInt(new MyInt(15))
printlnInt(new MyInt(15) + 12)

//printlnInt(12.2)  // error: expected Int
