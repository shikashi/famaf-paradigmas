var kMaxVerbosity = 5

def relog_byname(verbosity : Int, msg : => String) = {
    if (verbosity <= kMaxVerbosity)
        println("Log by name (%d): ".format(verbosity) + msg + "\n\tI repeat: " + msg);
}

def relog_lazy(verbosity : Int, msg : => String) = {
    lazy val lazy_msg = msg
    if (verbosity <= kMaxVerbosity)
        println("Log lazy (%d): ".format(verbosity) + lazy_msg + "\n\tI repeat: " + lazy_msg);
}

// no apparent difference: log and repeat
relog_byname(0, "meh")
relog_lazy(0, "meh")

// no difference: verbosity above maximum, print nothing
relog_byname(6, "nope")
relog_lazy(6, "nope")

// no difference: verbosity above maximum, Unit msg never evaluated
relog_byname(9, { println("Elided computation..."); Thread.sleep(8000); "costly msg" })
relog_lazy(9, { println("Elided computation..."); Thread.sleep(8000); "costly msg" })

// relog_byname: evaluate Unit msg 2 times
// relog_lazy: evaluate Unit msg only once
relog_byname(3, { println("Expensive computation..."); Thread.sleep(4000); "expensive msg" })
relog_lazy(3, { println("Expensive computation..."); Thread.sleep(4000); "expensive msg" })
