package objects;

public class Rock {
    public int weight;

    public Rock(int w) {
        this.weight = w;
    }

    protected boolean push() {
        if (this.isVeryHeavy()) {
            System.out.println("The rock is too heavy, it won't budge!");
            return false;
        } else {
            System.out.println("The rock moves.");
            return true;
        }
    }

    private boolean isVeryHeavy() {
        return this.weight > 100;
    }
}
