package objects;

public class Boulder extends Rock {
    public Boulder(int r) {
        super(r);
    }

    public void roll() {
        if (this.push())
            System.out.println("The rock was rolled!");
        //this.isVeryHeavy(); // error: isVeryHeavy() is private
    }
}
