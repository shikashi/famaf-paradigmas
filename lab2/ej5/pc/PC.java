package pc;

import objects.Boulder;

public class PC {
    String name;

    public PC(String name) {
        this.name = name;
    }

    public void interact(Boulder b) {
        System.out.println(this.name + " interacts with the boulder.");
        b.roll();
        //b.push(); // error: push() is protected
    }
}
