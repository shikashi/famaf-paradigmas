// Nota: Los argumentos se llaman como su nombre lo indica.
// (Functions, not arguments, are "called".)

public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Both parameters are instances of a class: arg1 and arg2 will be copies
     * of the references passed in.
     */
    public static void tricky1(Point arg1, Point arg2)
    {
        arg1.x = 100;  // modify the referred object
        arg1.y = 100;  // modify the referred object

        Point temp = arg1;  // a new local reference to arg1
        arg1 = arg2;  // change local reference
        arg2 = temp;  // change local reference
        // the previous 3 lines are useless
    }

    /**
     * Idem tricky1.
     */
    public static void tricky2(Point arg1 , Point arg2)
    {
        arg1 = null;  // useless: modify local reference
        arg2 = null;  // useless: modify local reference
    }

    public static void main(String[] args)
    {
        Point pnt1 = new Point (0, 0);
        Point pnt2 = new Point (0, 0);

        System.out.println("pnt1 X: " + pnt1.x + " pnt1 Y: " + pnt1.y);
        System.out.println("pnt2 X: " + pnt2.x + " pnt2 Y: " + pnt2.y);

        System.out.println("\ntricky1");
        tricky1(pnt1, pnt2);  // make pnt1.x == pnt1.y == 100, leave pnt2 unchanged
        System.out.println("pnt1 X: " + pnt1.x + " pnt1 Y: " + pnt1.y);
        System.out.println("pnt2 X: " + pnt2.x + " pnt2 Y: " + pnt2.y);

        System.out.println("\ntricky2");
        tricky2(pnt1, pnt2);  // useless: do nothing
        System.out.println("pnt1 X: " + pnt1.x + " pnt1 Y: " + pnt1.y);
        System.out.println("pnt2 X: " + pnt2.x + " pnt2 Y: " + pnt2.y);
    }
}
