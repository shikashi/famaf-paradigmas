#include <stdio.h>

void interchange(int, int);

int main(void)
{
    int x = 50, y = 70;

    printf("before: x=%d y=%d\n", x, y);
    interchange(x, y);  // useless: arguments passed by value
    printf("after: x=%d y=%d\n", x, y);
}

void interchange(int x1, int y1)
{
    int z1 = x1;
    x1 = y1;
    y1 = z1;

    printf("x1=%d y1=%d\n", x1, y1);
}
