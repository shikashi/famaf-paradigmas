$x = 50;
$y = 70;

print "interchange_not:\n";
print "before: x:$x y:$y\n";
&interchange_not($x, $y);  # useless: sub uses values
print "after: x:$x y:$y\n";

print "\n"

print "interchange_indeed:\n";
print "before: x:$x y:$y\n";
&interchange_indeed($x, $y);  # do swap: sub uses references
print "after: x:$x y:$y\n";

sub interchange_not {
    ($x1, $y1) = @_;

    $tmp = $x1;
    $x1 = $y1;
    $y1 = $tmp;

    print "x1:$x1 y1:$y1\n";
}

sub interchange_indeed {
    $tmp = $_[0];
    $_[0] = $_[1];
    $_[1] = $tmp;

    ($x1, $y1) = @_;
    print "x1:$x1 y1:$y1\n";
}
